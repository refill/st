﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Statki
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int[,] sm = new int[10, 10];
        public MainWindow()
        {
            InitializeComponent();
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    sm[i, j] = 0;
        }

        private void sm_button_Click(object sender, RoutedEventArgs e)
        {
            String[] tab = Convert.ToString(((Button)sender).Tag).Split('.');
            var button = sender as Button;
            if (sm[Convert.ToInt16(tab[0]), Convert.ToInt16(tab[1])] == 0)
            {
                button.Background = Brushes.Yellow;
                sm[Convert.ToInt16(tab[0]), Convert.ToInt16(tab[1])] = 1;
            }

            else
            {
                button.Background = new SolidColorBrush(Color.FromArgb(255, 170, 220, 249));
                sm[Convert.ToInt16(tab[0]), Convert.ToInt16(tab[1])] = 0;
            }
        }
    }
}
